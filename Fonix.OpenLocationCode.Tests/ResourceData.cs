﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Fonix.Tests
{
    internal static class ResourceData
    {
        public static string[] ReadAllLines( string resourceDataKey )
        {
            var assembly = Assembly.GetCallingAssembly();

            string resourceName = string.Empty;

            if ( !resourceDataKey.StartsWith( "." ) )
            {
                resourceName += ".Data.";
            }

            resourceName += resourceDataKey.Replace( "/", "." );

            string[] availableResources = assembly.GetManifestResourceNames();

            string matchingResourceName = availableResources.FirstOrDefault( ( x ) => x.EndsWith( resourceName ) );

            if ( matchingResourceName == null )
            {
                throw new ArgumentException( string.Format( "Unable to find file path for '{0}' in assembly resources!", resourceDataKey ) );
            }

            List<string> lines = new List<string>();

            using ( var stream = assembly.GetManifestResourceStream( matchingResourceName ) )
            {
                using ( var reader = new StreamReader( stream ) )
                {
                    string line;
                    while ( ( line = reader.ReadLine() ) != null )
                    {
                        if ( line.TrimStart().StartsWith( "#" ) )
                        {
                            continue;
                        }

                        lines.Add( line );
                    }
                }
            }

            return ( lines.ToArray() );
        }
    }
}
