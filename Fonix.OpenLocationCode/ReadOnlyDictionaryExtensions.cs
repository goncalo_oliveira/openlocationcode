﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fonix.OpenLocation
{
    internal static class ReadOnlyDictionaryExtensions
    {
        public static TValue? GetNullableValue<TKey, TValue>( this ReadOnlyDictionary<TKey, TValue> source, TKey key ) where TValue : struct, IComparable
        {
            if ( !source.ContainsKey( key ) )
            {
                return null;
            }

            return source[ key ];
        }
    }
}
